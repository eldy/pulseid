/// <reference types="Cypress" />

context('Gallery', () => {
	beforeEach(() => {
		cy.visit('http://localhost:3000')
	})

	const flickr_url = `https://api.flickr.com/services/rest/?api_key=${Cypress.env(
		'flickr_api_key'
	)}&format=json&method=flickr.photos.search&nojsoncallback=1&page=1&per_page=5&text=dog`

	describe('Form Elements', () => {
		it('should have a text input', () => {
			cy.get('input').should('match', 'input')
		})

		it('should have a dropdown', () => {
			cy.get('select').should('match', 'select')
		})
	})

	describe('Search photos', () => {
		it('should return 5 photos', () => {
			cy.get('input').type('dog')
			cy.get('form').submit()

			cy.server()
			cy.route({
				url: flickr_url,
				onRequest: () => {
					cy.get('#loading').should('have.length', 1)
				},
				onResponse: () => {
					cy.get('.photo').should('have.length', 5)
				}
			})
		})
	})

	describe('Search photos with page size filter', () => {
		it('should return 20 photos', () => {
			cy.get('input').type('dog')
			cy.get('form').submit()

			cy.get('select').select('20')

			cy.server()
			cy.route({
				url: flickr_url,
				onRequest: () => {
					cy.get('#loading').should('have.length', 1)
				},
				onResponse: () => {
					cy.get('.photo').should('have.length', 20)
				}
			})
		})
	})

	describe('Pagination', () => {
		it('should load photos of page 2', () => {
			const page = 2

			cy.get('input').type('dog')
			cy.get('form').submit()

			cy.get('.pagination')
				.contains(page)
				.click()

			cy.server()
			cy.route({
				url: `https://api.flickr.com/services/rest/?api_key=${Cypress.env(
					'flickr_api_key'
				)}&format=json&method=flickr.photos.search&nojsoncallback=1&page=${page}&per_page=5&text=dog`,
				onRequest: () => {
					cy.get('#loading').should('have.length', 1)
				},
				onResponse: response => {
					expect(response.body.photos.page).equal(page)
					cy.get('.photo').should('have.length', 5)
				}
			})
		})
	})
})
