import React, { useState, useEffect } from 'react'

export default ({ fetchPhotos, currentPage }) => {
	const [searchWord, setSearchWord] = useState('')
	const [perPageLimit, setPerPageLimit] = useState(5)

	const handleSearchSubmit = e => {
		e.preventDefault()

		if (searchWord) {
			fetchPhotos({
				searchWord,
				currentPage,
				perPageLimit
			})
		}
	}

	useEffect(() => {
		if (searchWord !== '') {
			fetchPhotos({
				searchWord,
				currentPage,
				perPageLimit
			})
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [currentPage, perPageLimit])

	return (
		<form onSubmit={handleSearchSubmit}>
			<input
				type="text"
				placeholder="Enter to search"
				onChange={e => setSearchWord(e.target.value)}
			/>
			<select onChange={e => setPerPageLimit(e.target.value)}>
				<option value="5">5</option>
				<option value="10">10</option>
				<option value="20">20</option>
			</select>
		</form>
	)
}
