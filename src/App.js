import React, { useState } from 'react'
import styled from 'styled-components'
import Photo from './Photo'
import chunk from 'lodash.chunk'

import { searchPhotos } from './Api'

import Pagination from './Pagination'
import Form from './Form'

const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
`

function App() {
  const [fetching, setFetching] = useState(false)
  const [photos, setPhotos] = useState([])

  //pagination
  const [pageCount, setPageCount] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)

  const fetchPhotos = async ({ searchWord, perPageLimit, currentPage }) => {
    setFetching(true)

    const photos = await searchPhotos({
      searchWord,
      perPageLimit,
      currentPage
    })

    setPhotos(photos.photo)
    setPageCount(Math.ceil(photos.total / perPageLimit))

    setFetching(false)
  }

  const handlePageClick = ({ selected }) => {
    const page = selected + 1
    setCurrentPage(page)
  }

  return (
    <div id='app'>
      <Form currentPage={currentPage} fetchPhotos={fetchPhotos} />

      <div>
        {fetching ? (
          <div id='loading'>...loading</div>
        ) : (
          chunk(photos, '3').map((row, i) => (
            <Row key={i}>
              {row.map(photo => (
                <Photo key={photo.id} {...photo} />
              ))}
            </Row>
          ))
        )}
      </div>

      {!!photos.length && (
        <Pagination handlePageClick={handlePageClick} pageCount={pageCount} />
      )}
      <br />
    </div>
  )
}

export default App
