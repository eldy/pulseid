import React from 'react'
import styled from 'styled-components'

const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin: 10px;

  img {
    align-self: center;
  }
`

export default ({ farm, server, id, secret, title }) => {
  const url = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_n.jpg`

  return (
    <Column className='photo'>
      <img src={url} alt={title} />
    </Column>
  )
}
