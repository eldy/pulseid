import React from 'react'
import Pagination from 'react-paginate'
import './pagination.css'

export default ({ handlePageClick, pageCount }) => (
	<Pagination
		previousLabel={'previous'}
		nextLabel={'next'}
		breakLabel={'...'}
		breakClassName={'break-me'}
		pageCount={pageCount}
		marginPagesDisplayed={2}
		pageRangeDisplayed={5}
		onPageChange={handlePageClick}
		containerClassName={'pagination'}
		subContainerClassName={'pages pagination'}
		activeClassName={'active'}
	/>
)
