import axios from 'axios'
import qs from 'query-string'

const searchPhotos = async ({ searchWord, perPageLimit, currentPage = 1 }) => {
	try {
		const reqParams = {
			api_key: 'b8d4d4790ef6b98bb88a3a095eaf6606',
			format: 'json',
			method: 'flickr.photos.search',
			nojsoncallback: 1,
			text: searchWord,
			per_page: perPageLimit,
			page: currentPage
		}

		const {
			data: { photos = {} }
		} = await axios.get(
			`https://api.flickr.com/services/rest/?${qs.stringify(reqParams)}`
		)

		return photos
	} catch (e) {
		console.log(e)
	}
}

export { searchPhotos }
